#[cfg(feature = "result")]
pub mod result;

/// Extensions for `Option<T>` where `T: Default`
pub trait OptionExtDefault<T> {
    /// If `self` is `Some(v)`, returns v, otherwise returns `Default::default()`
    fn or_default(self) -> T;
}

impl<T> OptionExtDefault<T> for Option<T> where
    T: Default,
{
    #[inline]
    fn or_default(self) -> T {
        self.unwrap_or_else(Default::default)
    }
}

#[cfg(feature = "result")]
impl<T, E> OptionExtDefault<T> for Result<T, E> where
    T: Default,
{
    #[inline]
    fn or_default(self) -> T {
        self.ok().or_default()
    }
}

pub trait TupleExtFlip<A, B>: Sized {
    fn flipped(self) -> (B, A);
}

impl<A, B> TupleExtFlip<A, B> for (A, B) {
    fn flipped(self) -> (B, A) {
        let (a, b) = self;
        (b, a)
    }
}

/// Extensions for Option<T> for dealing with tuples
pub trait OptionExtTup<T>: Sized {
    fn and_then_tup<F, Ret>(self, f: F) -> Option<(T, Ret)> where
        Ret: Sized,
        F: FnOnce(&T) -> Option<Ret>;
    fn and_then_tup_flipped<F, Ret>(self, f: F) -> Option<(Ret, T)> where
        Ret: Sized,
        F: FnOnce(&T) -> Option<Ret>
    {
        self.and_then_tup(f).map(TupleExtFlip::flipped)
    }
    fn and_tup<Ret>(self, v: Option<Ret>) -> Option<(T, Ret)> {
        self.and_then_tup(move |_| v)
    }
    fn and_tup_flipped<Ret>(self, v: Option<Ret>) -> Option<(Ret, T)> {
        self.and_tup(v).map(TupleExtFlip::flipped)
    }
}

impl<T: Sized> OptionExtTup<T> for Option<T> {
    fn and_then_tup<F, Ret>(self, f: F) -> Option<(T, Ret)> where
        Ret: Sized,
        F: FnOnce(&T) -> Option<Ret>,
    {
        self.and_then(move |v| f(&v).map(move |vv| (v, vv)))
    }
}
