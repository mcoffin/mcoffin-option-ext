use crate::TupleExtFlip;

pub trait ResultExtTup<T, E>: Sized {
    fn and_then_tup<F, Ret, EE>(self, f: F) -> Result<(T, Ret), E> where
        Ret: Sized,
        EE: Into<E>,
        F: FnOnce(&T) -> Result<Ret, EE>;
    fn and_then_tup_flipped<F, Ret, EE>(self, f: F) -> Result<(Ret, T), E> where
        Ret: Sized,
        EE: Into<E>,
        F: FnOnce(&T) -> Result<Ret, EE>
    {
        self.and_then_tup(f).map(TupleExtFlip::flipped)
    }
    fn and_tup<Ret, EE>(self, v: Result<Ret, EE>) -> Result<(T, Ret), E> where
        EE: Into<E>,
    {
        self.and_then_tup(move |_| v)
    }
    fn and_tup_flipped<Ret, EE: Into<E>>(self, v: Result<Ret, EE>) -> Result<(Ret, T), E> {
        self.and_tup(v).map(TupleExtFlip::flipped)
    }
}

impl<T: Sized, E: Sized> ResultExtTup<T, E> for Result<T, E> {
    fn and_then_tup<F, Ret, EE>(self, f: F) -> Result<(T, Ret), E> where
        Ret: Sized,
        EE: Into<E>,
        F: FnOnce(&T) -> Result<Ret, EE>,
    {
        self.and_then(move |v| f(&v).map_err(Into::into).map(move |vv| (v, vv)))
    }
}
